/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import java.awt.Cursor;
import java.sql.*;

/**
 *
 * @author mateo
 */
public abstract class dbManejador {
    protected Connection conexion;
    protected PreparedStatement sqlConsulta;
    protected ResultSet registros;
    private String usuario;
    private String database;
    private String contraseña;
    private String drive;
    private String url;
    
    public dbManejador(Connection conexion, PreparedStatement sqlConsulta, ResultSet registros, String usuario, String database, String contraseña, String drive, String url) {
        this.conexion = conexion;
        this.sqlConsulta = sqlConsulta;
        this.registros = registros;
        this.usuario = usuario;
        this.database = database;
        this.contraseña = contraseña;
        this.drive = drive;
        this.url = url;
        isDrive();
    }

    public dbManejador() {
        this.drive = "com.mysql.cj.jdbc.Driver";
        this.database = "sistema";
        this.usuario = "admin";
        this.contraseña = "ramon2008";
        this.url = "jdbc:mysql://microinformaticamx.cmwe0ss4z5wt.us-east-2.rds.amazonaws.com/sistema";
        isDrive();
    }
    
    /*public dbManejador() {
        this.drive = "com.mysql.jdbc.Driver";
        this.database = "sistemas";
        this.usuario = "root";
        this.contraseña = "";
        this.url = "jdbc:mysql://localhost/sistemas";
        isDrive();
    }*/

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public PreparedStatement getSqlConsulta() {
        return sqlConsulta;
    }

    public void setSqlConsulta(PreparedStatement sqlConsulta) {
        this.sqlConsulta = sqlConsulta;
    }

    public ResultSet getRegistros() {
        return registros;
    }

    public void setRegistros(ResultSet registros) {
        this.registros = registros;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getDrive() {
        return drive;
    }

    public void setDrive(String drive) {
        this.drive = drive;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    public boolean isDrive(){
        boolean exito = false;
        try{
            Class.forName(drive);
            exito = true;
        }
        catch(ClassNotFoundException e){
            exito = false;
            System.err.println("Surgió un error " + e.getMessage());
            System.exit(-1);
        }
        return exito;
    }
    
    public boolean conectar(){
        boolean exito = false;
        try{
            this.setConexion((DriverManager.getConnection(this.url, this.usuario, this.contraseña)));
            exito = true;
            
        }
        catch(SQLException e){
            exito = false;
            System.err.println("Surgió un error al conectar " + e.getMessage());
        }
        return exito;
    }
    
    public void desconectar(){
        try{
            if(!this.conexion.isClosed()){
                this.getConexion().close();
            }
        }
        catch(SQLException e){
            System.err.println("Error, no fue posible cerrar la conexion " + e.getMessage());
        }
    }
}