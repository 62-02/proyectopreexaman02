/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author mateo
 */
public class Usuarios {
    private int idUsuario;
    private String usuario;
    private String correo;
    private String ctr;
    private String ctr2;

    public Usuarios(int idUsuario, String usuario, String correo, String ctr, String ctr2) {
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.correo = correo;
        this.ctr = ctr;
        this.ctr2 = ctr2;
    }
    
    public Usuarios(){
        
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCtr() {
        return ctr;
    }

    public void setCtr(String ctr) {
        this.ctr = ctr;
    }

    public String getCtr2() {
        return ctr2;
    }

    public void setCtr2(String ctr2) {
        this.ctr2 = ctr2;
    }
    
    
    
}
