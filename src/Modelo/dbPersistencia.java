/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import java.util.ArrayList;

/**
 *
 * @author mateo
 */
public interface dbPersistencia {
    public void insertar(Object objeto) throws Exception;
    public void actualizar(Object objeto) throws Exception;
    public void borrar(Object objeto, String codigo) throws Exception;
    
    public boolean isExiste(String codigo) throws Exception;
    public ArrayList listar() throws Exception;
    
    public Object buscar(String codigo) throws Exception;
}
