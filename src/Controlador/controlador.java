package Controlador;
import Vista.vistaIngreso;
import Vista.vistaLista;
import Vista.vistaRegistro;
import Modelo.Usuarios;
import Modelo.dbUsuarios;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author mateo
 */

//SE TERMINO EL CONTROLADOR
public class controlador implements ActionListener{

    private vistaIngreso VI;
    private vistaLista VL;
    private vistaRegistro VR;
    private Usuarios U;
    private dbUsuarios UD;
    private boolean A = false;

    public controlador(Usuarios U, dbUsuarios UD, vistaIngreso VI, vistaLista VL, vistaRegistro VR) {
        this.U = U;
        this.UD = UD;
        this.VI = VI;
        this.VL = VL;
        this.VR = VR;
        
        VI.btnIngresarIn.addActionListener(this);
        VI.btnRegistrarIn.addActionListener(this);
        VL.btnCerrarUs.addActionListener(this);
        VR.btnCerrarR.addActionListener(this);
        VR.btnGuardarR.addActionListener(this);
        VR.btnLimpiarR.addActionListener(this);
    }

    private void iniciarIngreso() {
        limpiar();
        VI.setTitle(":: INGRESAR ::");
        VI.setSize(380, 300);
        VI.setLocationRelativeTo(null);
        VI.setVisible(true);
    }
    
    private void iniciarLista() {
        VL.setTitle(":: LISTA ::");
        VL.setSize(410, 360);
        VL.setLocationRelativeTo(null);
        VL.setVisible(true);
    }
    
    private void iniciarRegistro() {
        VR.setTitle(":: REGISTRAR ::");
        VR.setSize(400, 330);
        VR.setLocationRelativeTo(null);
        VR.setVisible(true);
    }
    
     private void cerrarIngreso() {
        VI.setVisible(false);
    }
    
    private void cerrarLista() {
        VL.setVisible(false);
    }
    
    private void cerrarRegistro() {
        VR.setVisible(false);
    }

    private void limpiar() {
        VR.txtContraseñaR.setText("");
        VR.txtCorreoElectronicoR.setText("");
        VR.txtReescribirContraseñaR.setText("");
        VR.txtUsuarioR.setText("");
        A = (false);
    }

    private void cargarDatos() {
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Usuarios> lista = new ArrayList<>();
        try {
            lista = UD.listar();
        } catch (Exception ex) {
        }
        modelo.addColumn("Id");
        modelo.addColumn("usuario");
        modelo.addColumn("correo");
        for (Usuarios producto : lista) {
            modelo.addRow(new Object[]{producto.getIdUsuario(), producto.getUsuario(), producto.getCorreo(), producto.getCtr()});
        }
        VL.jTabla1.setModel(modelo);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       
        if (e.getSource() == VI.btnIngresarIn) {
            try {
                Usuarios users = new Usuarios();
                if((UD.isExiste(VI.txtUsuarioIn.getText()))){
                    users = (Usuarios) UD.buscar(VI.txtUsuarioIn.getText());
                    if(VI.txtContraseñaIn.getText().equals(users.getCtr())){
                        JOptionPane.showMessageDialog(null, "Usted ha entrado");
                        cerrarIngreso();
                        iniciarLista();
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "La contraseña es incorrecta");
                        return;
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "No se encontro el usuario");
                    return;
                }
                
            } catch (SQLException d) {
                d.printStackTrace();
            } catch (Exception ex1) {
                System.err.println("Surgió un error" + ex1);
            }
        }
        
        if(e.getSource() == VI.btnRegistrarIn){
            int option = JOptionPane.showConfirmDialog(VI, "¿Desea crear un nuevo usuario?",
                    "Decide", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                VI.dispose();
                cerrarIngreso();
                iniciarRegistro();
            }
            
        }
        
        if (e.getSource() == VR.btnGuardarR) {
            try {
                
                if("".equalsIgnoreCase(VR.txtUsuarioR.getText()))
                {
                    throw new IllegalArgumentException("Escribe un usuario");
                }
                
                if((UD.isExiste(VR.txtUsuarioR.getText())))
                {
                    JOptionPane.showMessageDialog(VR, "Este usuario ya existe");
                    return;
                }
                
                if("".equalsIgnoreCase(VR.txtCorreoElectronicoR.getText()))
                {
                    throw new IllegalArgumentException("Escribe un correo");
                }
                
                if("".equalsIgnoreCase(VR.txtContraseñaR.getText()))
                {
                    throw new IllegalArgumentException("Escribe una contraseña");
                }
                
            
            }catch(IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(VR, "SURGIO UN ERROR: " + ex.getMessage());
                return;
            }catch (Exception ex2) {
                JOptionPane.showMessageDialog(VR, "Error: " + ex2.getMessage());
                return;
            }

            
            try{
                U.setUsuario(VR.txtUsuarioR.getText());
                U.setCorreo(VR.txtCorreoElectronicoR.getText());
                U.setCtr(VR.txtContraseñaR.getText());
                if(VR.txtContraseñaR.getText().equals(VR.txtReescribirContraseñaR.getText()))
                {
                    U.setCtr2(VR.txtReescribirContraseñaR.getText());
                }
                else{
                    JOptionPane.showMessageDialog(null, "Las contraseñas no coinciden");
                    return;
                }
                
                A = true;
                if (A) {
                    UD.insertar(U);
                    JOptionPane.showMessageDialog(null, "Registro Guardado");
                    limpiar();
                    cargarDatos();
                }
            } catch (SQLException d) {
                d.printStackTrace();
            } catch (Exception ex1) {
                System.err.println("Surgió un error" + ex1);
            }
        }
        
        if(e.getSource() == VR.btnLimpiarR){
            limpiar();
        }
        
        if(e.getSource() == VR.btnCerrarR){
             int option = JOptionPane.showConfirmDialog(VR, "¿Desea volver a la pantalla de Log In?",
                    "Decide", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                VR.dispose();
                cerrarRegistro();
                iniciarIngreso();
            }
        }
        
        if(e.getSource() == VL.btnCerrarUs){
             int option = JOptionPane.showConfirmDialog(VL, "¿Seguro que desea salir?",
                    "Decide", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                VL.dispose();
                System.exit(0);
                cerrarLista();
            }
        }
    }
        
    public static void main(String[] args) {
        Usuarios U = new Usuarios();
        dbUsuarios UD = new dbUsuarios();
        vistaIngreso VI = new vistaIngreso(new JFrame(), true);
        vistaLista VL = new vistaLista(new JFrame(), true);
        vistaRegistro VR = new vistaRegistro(new JFrame(), true);
        controlador contra = new controlador(U, UD, VI, VL, VR);
        contra.cargarDatos();
        contra.iniciarIngreso();
    }
}
